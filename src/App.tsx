import React, { useState } from "react";
import "./App.css";
import { createData, DataItem } from "./Data";
import { Paper, Collapse } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const data = createData(20);

const ExpandableRow: React.FC<{ item: DataItem }> = ({ item }) => {
  const [isVisible, setIsVisible] = useState(false);

  const toggleVisible = () => {
    setIsVisible(!isVisible);
  };

  return (
    <React.Fragment>
      <div className="App-Row" onClick={toggleVisible}>
        <div className="App-Cell">{item.id}</div>
        <div className="App-Cell">{item.name}</div>
        <div className="App-Cell">
          <ExpandMoreIcon
            className={isVisible ? "App-Icon-Rotate" : "App-Icon"}
          />
        </div>
      </div>
      <Collapse in={isVisible}>
        <div className="App-Row">
          <h1>I'm now visible</h1>
          <h3>This is my extra data</h3>
        </div>
      </Collapse>
    </React.Fragment>
  );
};

const App: React.FC = () => {
  return (
    <div className="App">
      <Paper>
        <div className="App-Table">
          <div className="App-Row">
            <div className="App-Cell">ID</div>
            <div className="App-Cell">Name</div>
            <div className="App-Cell" />
          </div>
        </div>
        {data.map(item => (
          <ExpandableRow key={item.id} item={item} />
        ))}
      </Paper>
    </div>
  );
};

export default App;
