export type DataItem = {
  id: number;
  name: string;
};

export function createData(amount: number): DataItem[] {
  const createItem = (idx: number) => ({ id: idx, name: `Row ${idx + 1}` });

  const data = [];

  for (let i = 0; i < amount; i += 1) {
    data.push(createItem(i));
  }

  return data;
}
